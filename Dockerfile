FROM alpine:3.15.4

# hadolint ignore=DL3001,DL3018,DL4001,DL4006
RUN set -eux \
  \
  ; echo 'Installing Helm...' >&2 \
  \
  ; export VERSION="3.8.1" \
  ; export BASEURL="https://get.helm.sh" \
  ; export PACKAGE="helm-v${VERSION}-linux-amd64.tar.gz" \
  ; export SHASUMS="${PACKAGE}.sha256sum" \
  ; wget -nv -O "$SHASUMS" "${BASEURL}/${SHASUMS}" \
  ; wget -nv -O "$PACKAGE" "${BASEURL}/${PACKAGE}" \
  ; grep "$PACKAGE" "$SHASUMS" | sha256sum -c - \
  ; tar -zxf "$PACKAGE" \
  ; rm -f "$PACKAGE" "$SHASUMS" \
  ; chmod 0755 ./linux-amd64/helm \
  ; mv -vf ./linux-amd64/helm /usr/bin/helm \
  ; rm -rf linux-amd64 \
  ; helm version --short \
  \
  ; echo 'Installing system packages...' >&2 \
  \
  ; apk add --no-cache \
        curl \
        git \
        openssh-client \
  ; curl --version \
  ; git --version \
  ; ssh -V \
  \
  ; echo 'Creating user and settings...' >&2 \
  \
  ; adduser -g CI -s /bin/ash -D -u 1042 ci \
  ; mkdir -p /home/ci/.ssh \
  ; echo 'StrictHostKeyChecking no'     >> /home/ci/.ssh/config \
  ; echo 'UserKnownHostsFile=/dev/null' >> /home/ci/.ssh/config \
  \
  ; echo 'Fixing permissions...' >&2 \
  \
  ; chown -R ci:ci /home/ci/.ssh \
  ; chmod 0700 /home/ci/.ssh

COPY --chown=root:root scripts/upload-chart.sh /usr/bin/upload-chart

RUN set -eux \
  ; update-ca-certificates \
  ; chmod 0755 /usr/bin/upload-chart

USER ci
